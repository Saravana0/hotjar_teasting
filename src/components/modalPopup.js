import { React, useState } from "react";
import { Button, Modal, Table } from "react-bootstrap";
import { FiSearch } from "react-icons/fi";

export default function ModalPopup(props) {
  console.log(props);

  return (
    <div>
      <Modal show={props.popup} size={props.size} centered>
        <div>
          <Modal.Header>
            <h4> {props.heading} </h4>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={() => props.close(false)}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </Modal.Header>
          <Modal.Body>{props.body}</Modal.Body>
          {/* <Modal.Footer>
                <Button  onClick={() => props.close(false)}>
                    Close Modal
                </Button>
            </Modal.Footer> */}
        </div>
      </Modal>
    </div>
  );
}
