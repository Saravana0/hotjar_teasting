import React, { Component, useState } from "react";
import DatePicker from "react-datepicker";
import { RiCalendarTodoFill } from "react-icons/ri";
import { IoMdDownload } from "react-icons/io";
import { AiOutlineDown } from "react-icons/ai";
import { Button, Form, Row, Col } from "react-bootstrap";
import { BiChevronDown } from "react-icons/bi";

function DateSelector() {
  const DateSelect = () => {
    const [dateRange, setDateRange] = useState([null, null]);
    const [startDate, endDate] = dateRange;

    return (
      <div className="filter-status-outline">
        <label className="datepicker-length">
          <DatePicker
            className="datepicker"
            selectsRange={true}
            startDate={startDate}
            endDate={endDate}
            onChange={(update) => {
              setDateRange(update);
            }}
            onKeyDown={(e) => {
              e.preventDefault();
            }}
            // isClearable={true}
            dateFormat="d MMM yyyy "
          >
            <div className="datepicker-button ">
              <button className="cancel-button"
              >cancel</button>
              <button
                className="choosedate-button"
                onClick={() => setDateRange([null, null])}
              >
                choose date
              </button>
            </div>
          </DatePicker>
          <BiChevronDown size={20} className="down-icon" />

          <RiCalendarTodoFill className="calendar-icon" />
        </label>
        {/* <AiOutlineDown /> */}
      </div>
    );
  };

  return (
    <>
      <DateSelect />
    </>
  );
}
export default DateSelector;
