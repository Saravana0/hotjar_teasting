import React, { Component, useState } from "react";
import DatePicker from "react-datepicker";
import { RiCalendarTodoFill } from "react-icons/ri";
import { IoMdDownload } from "react-icons/io";
import { AiOutlineDown } from "react-icons/ai";

function SingleDateSelector(props) {
  const SingleDateSelect = () => {
    const [date, setDate] = useState(null);

    const handleCalendarClose = () => console.log("Calendar closed");
    const handleCalendarOpen = () => console.log("Calendar opened");

    return (
      <DatePicker
        selected={date}
        onChange={(date) => setDate(date)}
        onCalendarClose={handleCalendarClose}
        onCalendarOpen={handleCalendarOpen}
        disabled={props.name === "View"}
      />
    );
  };

  return (
    <>
      <SingleDateSelect />
    </>
  );
}
export default SingleDateSelector;
