import React from "react";
// import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
// import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
// import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import BootstrapTable from "react-bootstrap-table-next";
import PaginatedItems from "../pagination/pagination";
// import  TableHeaderColumn  from 'react-bootstrap-table';
// import paginationFactory from "react-bootstrap-table2-paginator";
// import img1 from '../assests/view.png';
// import paginationFactory from "react-bootstrap-table2-paginator";
import {
  Button,
  Form,
  Toast,
  Row,
  Col,
  FormControl,
  InputGroup,
} from "react-bootstrap";
import { Link } from "react-router-dom";

import paginationFactory from "react-bootstrap-table2-paginator";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

// export default function MainTable(props) {
//   return (
//     <>
//       <section>
//         <div>
//           <div className="retailHeading">
//             <h2>{props.head}</h2>
//             {/* <h2>Retail store</h2> */}
//           </div>
//         </div>

//         <div className="searchbar">
//           <form class="form-inline">
//             <input
//               class="form-control mr-sm-2 search"
//               type="search"
//               placeholder="Search"
//               aria-label="Search"
//             ></input>
//           </form>
//           {props.buttonhead ? (
//             <div className="btn-one">
//               <button type="button">
//                 {/* Add Delivery partner */}
//                 {props.buttonhead}
//               </button>
//             </div>
//           ) : null}
//         </div>

//         <div className="table-one">
//           <BootstrapTable
//             keyField="id"
//             data={props.data}
//             columns={props.columns}
//             bordered={false}
//             bootstrap4
//             wrapperClasses="table-responsive"
//             rowClasses="text-nowrap"
//           />
//         </div>
//       </section>

export default function MainTable(props) {
  //   const pagintnoptions = {
  //     // pageStartIndex: 0,
  //     sizePerPage: 15,
  //     hideSizePerPage: true,
  //     hidePageListOnlyOnePage: true,
  //   };

  const formatDatas = (initArr) => {
    let sn = 0;
    let filteredarray = [];

    initArr.forEach((element) => {
      const offVal = (props.offset - 1) * 10 + (sn + 1);
      const offsetValue = props.offset ? offVal : sn + 1;
      console.log(element);

      filteredarray.push({
        ...element,
        sno: offsetValue,

        // action: element.id,
      });
      console.log(filteredarray);
      sn++;
    });
    return filteredarray;
  };
  // // console.log(props,'props')
  const data = formatDatas(props.data);
  console.log(data.sno + "gdfgd");

  const customTotal = (from, to, size) => (
    <>
      
      <span className="react-bootstrap-table-pagination-total totalpage">
        {from} to {to} of {size}
      </span>
    </>
  );

  const options = {
    paginationSize: 4,
    pageStartIndex: 1,
    alwaysShowAllBtns: true, // Always show next and previous button
    withFirstAndLast: true, // Hide the going to First and Last page button
    // hideSizePerPage: true, // Hide the sizePerPage dropdown always
    //  hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
    // firstPageText: '<<',
    // prePageText: '<',
    // nextPageText: '>',
    // lastPageText: '>>',
    // nextPageTitle: 'First page',
    // prePageTitle: 'Pre page',
    // firstPageTitle: 'Next page',
    // lastPageTitle: 'Last page',
    showTotal: true,
    paginationTotalRenderer: customTotal,
    disablePageTitle: true,

    sizePerPageList: [
      {
        text: "5",
        value: 5,
      },
      {
        text: "10",
        value: 10,
      },
      {
        text: "15",
        value: 15,
      },
    ], // A numeric array is also available. the purpose of above example is custom the text
  };
  const dummy = [
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "black",
      value: "#000",
    },
    {
      color: "red",
      value: "#f00",
    },
    {
      color: "green",
      value: "#0f0",
    },
    {
      color: "blue",
      value: "#00f",
    },
    {
      color: "cyan",
      value: "#0ff",
    },
    {
      color: "magenta",
      value: "#f0f",
    },
    {
      color: "yellow",
      value: "#ff0",
    },
    {
      color: "black",
      value: "#000",
    },
  ];

  return (
    <>
      <div className="fixedHeightTable">
        <div className={props.popup ? "customTable" : "customTable mt-5 "}>
          <div>
            <BootstrapTable
              keyField="id"
              bordered={false}
              data={data}
              columns={props.columns}
              bootstrap4={true}
              headerWrapperClasses="thead-dark"
              bodyClasses="tableBody"
              wrapperClasses="table-responsive customScroll"
              // pagination={ paginationFactory(options) }
            />
          </div>
          {props.hidePagination ? (
            ""
          ) : (
            <div className="p-2 pagination-content">
              <div className="d-flex  align-items-center  per-page">
                <label className="page">Rows per page: &nbsp;</label>
                <div>
                  <Form.Control
                    as="select"
                    name="workout_category_id"
                    className="w-100"
                  >
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                  </Form.Control>
                </div>
              </div>

              <div className="pagenationwrapper d-flex ">
                <div className="text-end p-2 mr-4 page"> 1-10 of 275</div>
                <PaginatedItems
                  itemsPerPage={4}
                  item={dummy}
                  setpages={props.setPage}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
}
