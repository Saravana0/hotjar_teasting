import React from "react";
import octoLogo from "../assests/octoLogo.png";
import { AiFillBell } from "react-icons/ai";
import { NavLink, Link } from "react-router-dom";

import MainTable from "../components/tables/mainTable";
import { Button, Modal, Table, Form, Col, Row } from "react-bootstrap";
import { BsFillEyeFill } from "react-icons/bs";
import { Component, useState, useRef } from "react";
import ViewModalPopup from "../components/modalPopup";


export default function Header() {

  // const [userModaTable, setUserModalTable] = useState(false);
  // const [userModalTables, setUserModalTables] = useState(false);

  // const viewRetailModal = (e) => {
  //   setUserModalTable(!userModaTable);
  //   setUserModalTables(false);
   
  // };


  function btnFormatter(cell, row, id) {
    return (
      <>

        <div className="eye_button">

          <div className="eye_icon" onClick={()=>viewRetailModal()}>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <BsFillEyeFill />
          </div>

          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

          <div className="order_button headModalBtnHoverBg">
            {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
            <Button variant="success">Order Ready</Button>{" "}
          </div>

        </div>
      </>
    );
  }

  const data = [
    {
      s_no: 1,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 2,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 3,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 4,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 5,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 6,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 7,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 8,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 9,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },
    {
      s_no: 10,
      order_no: "#12345",
      customer_name: "Justin Septimus",
      mobile_no: "9999999999",
      status: "Order accepted",
      delivery_partner_name: "Bharath Raj",
      mobile_no: "9999999999",
      action: "Action",
    },

  ];
  const columns = [
    {
      dataField: "s_no",
      text: "S.no",
    },
    {
      dataField: "order_no",
      text: "Order no",
    },
    {
      dataField: "customer_name",
      text: "Customer name",
    },
    {
      dataField: "mobile_no",
      text: "Mobile number",
    },
    {
      dataField: "status",
      text: "Status",
    },
    {
      dataField: "delivery_partner_name",
      text: "Delivery Partner name",
    },
    {
      dataField: "mobile_no",
      text: "Mobile number",
    },
    {
      dataField: "action",
      text: "Action",
      formatter: btnFormatter,
    },
  ];
  const [inprogressOrder, setInprogressOrder] = useState(false);

  const inprogressOrderModal = () => {
    setInprogressOrder(true);

  };

  const inProgressContent = () => {
    return (
      <>
        <div className="assain_retail">
          <MainTable
            data={data}
            columns={columns}
            hidePagination={true}
            popup={true}
          />
          {/* <TableSelect
              clickSelect="true"
              data={tableList}
              offset={filter ? offset : offset}
              // resetSelectRow={resetSelectRow}
              ref={tableRef}
              tableCount={tableCount}
              showAllRecords={showAllRecords}
              showButton={showButton}
              // columns={columnss}
              // filters={tableFilters}
              // getSelectedRow={getSelectedRow}
              searchBar="false"
            /> */}
        </div>
      </>
    );
  };
  const [viewRetailPopup, setViewRetailPopup] = useState(false);

  const retailerLocation = [
    {
      id: 0,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 1,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 2,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 3,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
  ];


  const bodyContent = () => {
    return (
      <>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-1 flex-sm">
          <p className="color-text order-text"> Order ID : 1001 </p>
          <p className="order-date"> Order Date and Time:10 jan 2022,2:00pm </p>
        </div>
        <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3 flex-sm ">
          <p className="order-date"> Customer name: Raj </p>
          <p className="order-date">
            {" "}
            Mobile: <span> 9898978938 </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Email: <span> raj@email.com </span>{" "}
          </p>
        </div>

        <div className="d-flex justify-content-between mr-3 ml-3 mb-5 product-details customScroll">
          <table>
            <tr>
              <th>S.no</th>
              <th>Product name</th>
              <th>MRP</th>
              <th>Quantity</th>
              <th>Amount</th>
            </tr>
            {retailerLocation.map((item) => (
              <tr>
                <td>{item.id + 1} </td>
                <td>{item.productName}</td>
                <td>{item.mrp}</td>
                <td>{item.quantity}</td>
                <td>{item.amount}</td>
              </tr>
            ))}
            <tr className="total-txt pr-5">
              <td colspan="4" className="lable p-2">
                Total amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </td>
              <td>Rs.250</td>
            </tr>
          </table>
        </div>
      </>
    );
  };

  const viewRetailModal = () => {
    setViewRetailPopup(true);
    setInprogressOrder(false);
    
  };

  return (
    <>
      <div className="d-flex headed">
        <div className="head">
          <div className="logo">
            <img src={octoLogo} />
          </div>
        </div>
        <section className="rightContents">
          <div className="header ">
            <span className="headerinprogress">
              Today’s inprogress:
              <Link className="view" onClick={() => inprogressOrderModal()}>
                View
              </Link>
            </span>
            <button className="dot ml-3">D</button>
            <div className="pro-name pb-">
              <p>David</p>
              <p>super admin</p>
            </div>
          </div>
        </section>
        {inprogressOrder && (
          <ViewModalPopup
            close={setInprogressOrder}
            popup={inprogressOrder}
            heading={"Inprogress Order"}
            body={inProgressContent()}
            size={"xl"}
          />
        )}

        {viewRetailPopup && (
          <ViewModalPopup
            close={setViewRetailPopup}
            popup={viewRetailPopup}
            heading={"View order"}
            body={bodyContent()}
            size={"lg"}
          />
        )}
      </div>
    </>
  );
}
