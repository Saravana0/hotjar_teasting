// import Pagination from 'react-bootstrap/Pagination'
import { Divide } from "hamburger-react";
import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import ReactPaginate from "react-paginate";

// export default function Pagination(props) {

//      const [currentItems, setCurrentItems] = useState(null);
//      const [pageCount, setPageCount] = useState(0);
//      const [itemsPerPage, setitemsPerPage] = useState(10);
//      const [items, setitems] = useState(100);

//      const [itemOffset, setItemOffset] = useState(0);

//   const handlePageClick = (event) => {
//     const newOffset = (event.selected * itemsPerPage) % items;
//     console.log(
//       `User requested page number ${event.selected}, which is offset ${newOffset}`
//     );
//     setItemOffset(newOffset);
//   };
//     return (
//       <>
//         <div className="table-one-pagination">
//           <div className="row-per-page">
//             <label for="cars">Rows per page: &nbsp;</label>
//             <select name="cars" id="cars">
//               <option value="volvo">5</option>
//               <option value="saab">10</option>
//               <option value="opel">15</option>
//               <option value="audi">20</option>
//             </select>
//           </div>

//           <div>
//             <div className="retail_pagination"></div>
//           </div>

//           <ReactPaginate
//             previousLabel="<< <"
//             nextLabel=">> >"
//             pageClassName="page-item"
//             pageLinkClassName="page-link"
//             previousClassName="page-item"
//             previousLinkClassName="page-link"
//             nextClassName="page-item"
//             nextLinkClassName="page-link"
//             breakLabel=""
//             breakClassName="page-item"
//             breakLinkClassName="page-link"
//             pageCount={pageCount}
//             marginPagesDisplayed={0}
//             pageRangeDisplayed={3}
//             containerClassName="pagination"
//             activeClassName="active"
//             onPageChange={handlePageClick}
//             renderOnZeroPageCount={null}
//           />

//           {/* <div className='drop-down-row'>

//                     <label for="cars">Rows per page: &nbsp;</label>
//                     <select name="cars" id="cars">
//                         <option value="volvo">5</option>
//                         <option value="saab">10</option>
//                         <option value="opel">15</option>
//                         <option value="audi">20</option>
//                     </select>

//                     <div className='drop-down-pages'>

//                     </div>
//                 </div> */}

//           {/* <div className='retail_pagination' >
//                     <nav aria-label="Page navigation example">
//                         <ul class="pagination">
//                             <li class="page-item">
//                                 <a class="page-link" href="#" aria-label="Previous">
//                                     <span aria-hidden="true">&laquo;</span>
//                                     <span class="sr-only">Previous</span>
//                                     <span class="glyphicon glyphicon-fast-backward"></span>
//                                 </a>
//                             </li>
//                             <li class="page-item">
//                                 <a class="page-link" href="#" aria-label="Previous">
//                                     <span aria-hidden="true">	&lt;</span>
//                                     <span class="glyphicon glyphicon-chevron-left sr-only"></span>
//                                     <span class="sr-only">Previous</span>
//                                 </a>
//                             </li>
//                             <li class="page-item"><a class="page-link" href="#">1</a></li>
//                             <li class="page-item"><a class="page-link" href="#">2</a></li>
//                             <li class="page-item"><a class="page-link" href="#">3</a></li>
//                             <li class="page-item">
//                                 <a class="page-link" href="#" aria-label="Next">
//                                     <span aria-hidden="true">&gt;</span>
//                                     <span class="sr-only">Next</span>
//                                 </a>
//                             </li>
//                             <li class="page-item">
//                                 <a class="page-link" href="#" aria-label="Next">
//                                     <span aria-hidden="true">&raquo;</span>
//                                     <span class="sr-only">Next</span>
//                                 </a>
//                             </li>
//                         </ul>
//                     </nav>
//                 </div> */}
//         </div>
//       </>
//     );
// }const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];


export default function PaginatedItems({ itemsPerPage, item, setpages }) {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const [currentpage ,setCurrenPage] = useState(0);
  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(item.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(item.length / itemsPerPage));
  }, [itemOffset, itemsPerPage]);

  const handlePageClick = (event) => {
    setpages(event.selected);
    setCurrenPage(event.selected);
    const newOffset = (event.selected * itemsPerPage) % item.length;
    setItemOffset(newOffset);
  };
  

  return (
    <>
      <div className="d-flex align-items-start justify-content-center  ">
        <div className="page__arrow" onClick={() => setCurrenPage(0)}>
          &lt;&lt;
        </div>
        <ReactPaginate
          breakLabel=""
          nextLabel="> "
          onPageChange={handlePageClick}
          marginPagesDisplayed={0}
          pageRangeDisplayed={3}
          pageCount={pageCount}
          previousLabel=" <"
          pageClassName="page"
          pageLinkClassName="page"
          previousClassName="page"
          previousLinkClassName="page"
          nextClassName="page"
          nextLinkClassName="page"
          renderOnZeroPageCount={null}
          forcePage={currentpage}
        />
        <div className="page__arrow" onClick={() => setCurrenPage(pageCount)}>
          &gt;&gt;
        </div>
      </div>
    </>
  );
}
