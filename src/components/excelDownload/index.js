import React from "react";
import { Button } from "react-bootstrap";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";
import { MdDownload } from "react-icons/md";
import Buttons from "../button/button";
import { IoMdDownload } from "react-icons/io";

const ExportToExcel = ({ apiData, fileName }) => {
  const fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  const fileExtension = ".xlsx";

  const exportToCSV = (apiData, fileName) => {
    const ws = XLSX.utils.json_to_sheet(apiData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, fileName + fileExtension);
  };

  return (
    // <Button onClick={(e) => exportToCSV(apiData, fileName)} bg="bg-blue" size="sm">
    //   <MdDownload />
    // </Button>
    // <Button
    //   variant="secondary"
    //   size="sm"
    //   onClick={(e) => exportToCSV(apiData, fileName)}
    //   className=" text-center download-btn bg-blue p-2"
    // >
    //   <MdDownload size={20} />
    // </Button>
    <button
      className="dwnld-icon"
      onClick={(e) => exportToCSV(apiData, fileName)}
    >
      {" "}
      <IoMdDownload />
    </button>
  );
};
export default ExportToExcel;
