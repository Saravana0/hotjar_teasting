import React from 'react'
import SideBar from '../sidebar';

export default function Menu({children}) {
  return (
    
    < div className="layoutWrapper ">
      < div className="navbarSection" >
        <SideBar />
      </div>
    </div>
  );
}
  

