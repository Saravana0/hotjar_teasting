import React from "react";

export default function SummaryCard(props) {
  return (
    <div className="summary-wrapper  mb-3 mt-2">
      {props.data.map((item) => {
        return (
          <div className="summary-card">
            <p className="summary-head">{item.title}</p>
            <div className="summary-count">{item.count}</div>
          </div>
        );
      })}
    </div>
  );
}
