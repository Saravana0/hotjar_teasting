import React from "react";
import octoLogo from "../../assests/OCTO.png";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { Form, Button } from "react-bootstrap";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Buttons from "../button/button";

function Login() {
  const [pwd, setPwd] = useState("");
  const [email, setEmail] = useState("");
  const [isRevealPwd, setIsRevealPwd] = useState(false);
  let navigate = useHistory();
  const [emailerror, setEmailError] = useState(false);
  const [passerror, setPassError] = useState(false);
  const [error, setError] = useState({
    email:"",
    password:""
  });

  let regex = new RegExp("[a-z0-9]+@[a-z]+.[a-z]{2,3}");
  const validateEmail = (email) => {
    return regex.test(email);
  };

  const handelSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const emailCheck = validateEmail(email);

    if (!emailCheck) {
      setEmailError(true);
    }
    else{
    setEmailError(false);

    }
    if (pwd.length < 8) {
       setPassError(true);
    }else{
    setPassError(false);

    }

    if (emailCheck && pwd.length > 7) {
      setEmailError(false);
      setPassError(false);
      navigate.push("/Dashboard");
    }
  };

  return (
    <div className="admin">
      <div className="Octo">
        <img src={octoLogo} />
        <div className="ordered">
          <p>Just what the doctor ordered !</p>
        </div>
        <br></br>
        <div className=" login-box-center">
          <div className="login">
            <p>Login</p>

            <Form className="email " onSubmit={handelSubmit}>
              <Form.Group controlId="email" className="mb-4">
                <Form.Control
                  type="text"
                  name="email"
                  onChange={(e) => setEmail(e.target.value)}
                  required
                  className={emailerror ? "error " : " "}
                  placeholder="Email*"
                />
                <Form.Text className="text-muted">
                  {emailerror && (
                    <span className="error-msg">Please check email</span>
                  )}
                </Form.Text>
              </Form.Group>

              <Form.Group className="password mb-0" controlId="password">
                <Form.Control
                  type={isRevealPwd ? "text" : "password"}
                  value={pwd}
                  onChange={(e) => setPwd(e.target.value)}
                  name="password"
                  required
                  className={passerror ? "error " : " "}
                  placeholder="Password*"
                />

                <span
                  className="eye"
                  onClick={() => setIsRevealPwd((prevState) => !prevState)}
                >
                  {isRevealPwd ? <AiFillEye /> : <AiFillEyeInvisible />}
                </span>

                <Form.Text className="text-muted">
                  {passerror && (
                    <span className="error-msg">Please check password</span>
                  )}
                </Form.Text>
              </Form.Group>

              {/* <div className="submit">
                <button type="submit" onClick={submit}>
                  Login
                </button>
              </div> */}
              <Button
                variant="primary"
                type="submit"
                className="w-100 login-btn "
              >
                Login
              </Button>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Login;
