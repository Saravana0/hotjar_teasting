import React, { useState } from "react";
import MainTable from "../components/tables/mainTable";
import MainPages from "./mainPages";
import ExportToExcel from "../components/excelDownload/index";
import BootstrapTable from "react-bootstrap-table-next";
import Pagination from "../components/pagination/pagination";
import DateSelect from "../pages/reportDeliveryPartner";
import DateSelector from "../components/tables/datePicker";
import ViewModalPopup from "../components/modalPopup";
import { IoMdCloudUpload } from "react-icons/io";
import { FaSearch } from "react-icons/fa";
import Buttons from "../components/button/button";
import { Button, Form, Row, Col } from "react-bootstrap";
import { BsFillEyeFill } from "react-icons/bs";
import { FaPen } from "react-icons/fa";
import { NavLink, Link } from "react-router-dom";

function userButtonFormatter(cell, row) {
  return (
    <>
      <div className="d-flex align-items-center">
        <Button variant="link" dataid={cell}>
          <FaPen />{" "}
        </Button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {/* <Link
        onClick={() => {
          setAddCoupon(true);
          addCategoryModal("Edit");
        }}
        // variant="link"
        // to={{
        //   pathname: `/manage-workout/add-workout/`,
        //   state: {
        //     workout_master_id: row.id,
        //     workout_category_name: row.workout_category_name,
        //     edit: false,
        //     duplicate: false,
        //   },
        // }}
      >
        <FaPen />
      </Link> */}
        {/* <Form.Check
        type="switch"
        id={`toggle-switch-${cell}`}
        checked={row.is_active == 0 ? false : true}
        // onChange={() => userStatusChange(cell, row.is_active)}
      /> */}
        <div class="custom-control custom-switch toggle-switch">
          <input
            type="checkbox"
            class="custom-control-input"
            id="customSwitches"
          />
          <label class="custom-control-label" for="customSwitches"></label>
        </div>
      </div>
    </>
  );
}

function RetailStore() {
  const [addCategory, setAddCategory] = useState(false);
  const [addCoupon, setAddCoupon] = useState(false);
  const [lable, setLable] = useState();

  const addCategoryModal = (name) => {
    setAddCategory(true);
    setLable(name);
  };

  const addCategoryContent = (name) => {
    return (
      <Form>
        <Form.Group as={Row} className="mb-3 mb-1">
          <Form.Label column sm="3">
            Category name:
          </Form.Label>
          <Col sm="7">
            <Form.Control type="text" placeholder="" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Add image:
          </Form.Label>
          <Col
            className="ml-4"
            sm="3"
            className={
              "image"
                ? "imgUploadBox d-flex align-items-center justify-content-center mwx-400"
                : "imgUploadBox d-flex align-items-center justify-content-center mwx-400 ml-auto mr-auto "
            }
          >
            <input type="file" name="avatar" accept="image/*" />
            <IoMdCloudUpload />
          </Col>
        </Form.Group>

        <div className="d-flex justify-content-end modalBtnHoverBg">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg="m-4 save" name="Save" />
            </>
          )}

          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel edit-cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg=" m-4 save edit-save" name="Update" />
            </>
          )}
        </div>
      </Form>
    );
  };

  const head = "Retail Store";
  const buttonhead = "Payment Completed";
  const data = [
    {
      s_no: 1,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
      
    },
    {
      s_no: 2,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,

    },
    {
      s_no: 3,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 4,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 5,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 6,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 7,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {

      s_no: 8,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 9,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 10,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 11,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 12,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 13,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 14,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    {
      s_no: 15,
      category_id: "C0001",
      category_name: 'Category 1',
      image: <Link className="image">Image1.png</Link>,
    },
    
  ];

const columns = [{
    dataField: 's_no',
    text: 'S.no',
  }, {
    dataField: 'category_id',
    text: 'Category ID'
  }, 
  {
    dataField: 'category_name',
    text: 'Category name'
  },
  {
    dataField: 'image',
    text: 'Image'
  },
  {
    dataField: 'action',
    text: 'Action',
    formatter: userButtonFormatter,
  
  },];

  function userButtonFormatter(cell, row) {
    return (
      <>
        <div className="d-flex justify-content-center align-items-center  ">
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          <Button
            className="border-none"
            variant="link"
            dataid={cell}
            onClick={() => addCategoryModal("Edit")}
          >
            <FaPen />{" "}
          </Button>
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          {/* <Link
          onClick={() => {
            setAddCoupon(true);
            addCategoryModal("Edit");
          }}
          // variant="link"
          // to={{
          //   pathname: `/manage-workout/add-workout/`,
          //   state: {
          //     workout_master_id: row.id,
          //     workout_category_name: row.workout_category_name,
          //     edit: false,
          //     duplicate: false,
          //   },
          // }}
        >
          <FaPen />
        </Link> */}

          {/* <Form.Check
          type="switch"
          id={`toggle-switch-${cell}`}
          checked={row.is_active == 0 ? false : true}
          // onChange={() => userStatusChange(cell, row.is_active)}
        /> */}
          <div class="custom-control custom-switch toggle-switch">
            <input
              type="checkbox"
              class="custom-control-input"
              id="customSwitches"
            />
            <label class="custom-control-label" for="customSwitches"></label>
          </div>
        </div>
      </>
    );
  }
  return (
    <>
      <div className="p-3 ">
        <div className="retailHeading ">
          <p>Category</p>
        </div>
      </div>
      <div className="search-button-table mt-3 ml-4 m-3">
        <div className=" d-flex justify-content-between search-border mainButtonHoverBg">
          <div class="main search retail ">
            <div class="form-group has-search pt-4">
              <span class="form-control-feedback">
                <FaSearch size={20} />
              </span>
              <input type="search" class="form-control" placeholder="Search" />
            </div>
          </div>

          <Buttons
            bg="bg-white mt-3 mr-2 w-60 font"
            name="Add Category"
            onClick={() => addCategoryModal("Add")}
          />
        </div>
        <MainTable data={data} columns={columns} />
        {addCategory && (
          <ViewModalPopup
            close={setAddCategory}
            popup={addCategory}
            heading={`${lable}  category`}
            body={addCategoryContent(lable)}
            size={"lg"}
          />
        )}
      </div>
    </>
  );
}
export default RetailStore;
