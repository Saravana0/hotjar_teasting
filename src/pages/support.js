import React, { Component, useState } from "react";
import MainTable from "../components/tables/mainTable";
import MainPages from "./mainPages";
import ExportToExcel from "../components/excelDownload/index";
import BootstrapTable from "react-bootstrap-table-next";
import Pagination from "../components/pagination/pagination";
import DateSelect from "../pages/reportDeliveryPartner";
import DateSelector from "../components/tables/datePicker";
import { Button, Modal, Table, Form, Col, Row } from "react-bootstrap";
import Buttons from "../components/button/button";
import { MdDelete } from "react-icons/md";
import { BsFillEyeFill } from "react-icons/bs";
import ViewModalPopup from "../components/modalPopup";
import { FaPen } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import { FaTrash } from "react-icons/fa";
function RetailStore() {
  const head = "Retail Store";
  const buttonhead = "Payment Completed";
  const [viewTicketPopup, setViewTicketPopup] = useState(false);
  const [lable, setLable] = useState();
  const viewTicketModal = () => {
    setViewTicketPopup(true);
  };
  /*---------------------View Ticket Content------------------------------ */
  const viewTicketContent = () => {
    return (
      <>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-1 card-content">
          <p className="color-text order-text"> ID: T0001 </p>
          <p className="order-date"> Ticket raised date : 15 Feb2022 </p>
        </div>
        <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3 card-content">
          <p className="order-date">
            {" "}
            Customer name: <span> Raj </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Mobile: <span> 9898978938 </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Email: <span> raj@email.com </span>{" "}
          </p>
        </div>
        <Form className="ml-3 mb-1">
          <Form.Group
            as={Row}
            className="mb-3 ml-0"
            controlId="formPlaintextPassword"
          >
            <Form.Label sm="3" className="retailStore-text">
              Product name
            </Form.Label>
            <Col sm="7">
              <Form.Control
                type="text"
                placeholder="Product 1"
                className="input-background"
                disabled
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3 ml-0"
            controlId="formPlaintextPassword"
          >
            <Form.Label sm="3" className="retailStore-text">
              Description
            </Form.Label>
            <Col sm="7" className="ml-4">
              <Form.Control
                as="textarea"
                rows={5}
                className="input-background text-area-font-size"
                placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi sse 1"
                disabled
              />
            </Col>
          </Form.Group>
        </Form>
        <Button className="float-right p-2 pl-5 pr-5 mb-5 mt-4 mr-3 close-btn-bgColor">
          {" "}
          Update{" "}
        </Button>
      </>
    );
  };
  function userButtonFormatterView(cell, row) {
    return (
      <>
        <div className="d-flex align-items-center">
          <Button
            className="border-none"
            variant="link"
            dataid={cell}
            onClick={() => viewTicketModal()}
          >
            <BsFillEyeFill />
          </Button>
          <Button
            className="border-none"
            variant="link"
            dataid={cell}
            // onClick={() => {
            //   setAddCoupon(true);
            //   setLable("Edit");
            // }}
          >
            <MdDelete size={18} />{" "}
          </Button>
        </div>
      </>
    );
  }
  const data = [
    {
      s_no: "1",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "2",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "3",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "4",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "5",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "6",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "7",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "8",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "9",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "10",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "11",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "12",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "13",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "14",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
    {
      s_no: "15",
      ticket_Id: "T0001",
      ticket_date: "15 FEB 2022",
      customer_name: "Justin Septimus",
      mobile_no: "99999999999",
      e_mail: "justin@gmail.com",
      product_name: "Product 1",
    },
  ];
  const columns = [
    {
      dataField: "s_no",
      text: "S.no",
    },
    {
      dataField: "ticket_Id",
      text: "Ticket ID",
    },
    {
      dataField: "ticket_date",
      text: "Ticket raised date",
    },
    ,
    {
      dataField: "customer_name",
      text: "Customer date",
    },
    {
      dataField: "mobile_no",
      text: "Mobile number",
    },
    {
      dataField: "e_mail",
      text: "Email ID",
    },
    {
      dataField: "product_name",
      text: "Product name",
    },
    {
      dataField: "action",
      text: "Action",
      formatter: userButtonFormatterView,
    },
  ];
  return (
    <>
      <div className="p-3 ">
        <div className="retailHeading ">
          <p>Support</p>
        </div>
      </div>
      <div className="search-button-table  mt-3 ml-4 m-3">
        <div className=" d-flex justify-content-between search-border">
          <div class="main search retail ">
            <div class="form-group has-search pt-4 pb-2">
              <span class="form-control-feedback">
                <FaSearch size={20} />
              </span>
              <input type="search" class="form-control" placeholder="Search" />
            </div>
          </div>
        </div>
        <div className="manage">
          <MainTable data={data} columns={columns} />
        </div>
        {viewTicketPopup && (
          <ViewModalPopup
            close={setViewTicketPopup}
            popup={viewTicketPopup}
            heading={"View Ticket"}
            body={viewTicketContent()}
            size={"lg"}
          />
        )}
      </div>
    </>
  );
}
export default RetailStore;
