import React from "react";
import { Tab, Col, Row, Nav } from "react-bootstrap";
import OrderCard from "../components/card/ordercard";
import NewRequestCard from "../components/card/newRequestCard";
import SummaryCard from "../components/summaryCard/summaryCard";


export default function Dashboard() {
  let summaryData = [
    {
      title: "Overall delivered",
      count: 100,
    },
    {
      title: "Delivered by you",
      count: 200,
    },
    {
      title: "Inprogress",
      count: 300,
    },
    {
      title: "Return",
      count: 400,
    },
  ];
  return (
    <>
      <div className="p-3">
        <div className="summary ">
          <div className="summary-title pb-2">
           Daily order summary <div className="span">22 Feb 2022</div>
          </div>
          <div className="mb-1">
            <SummaryCard data={summaryData} />
          </div>
        </div>
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <Row className="tabs mt-2">
            <Col xs lg="4" md="4" className="p-0">
              <Nav variant="pills" className="">
                <Nav.Item>
                  <Nav.Link eventKey="first">Order Details</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col xs lg="4" md="4" className="p-0">
              <Nav variant="pills" className="">
                <Nav.Item>
                  <Nav.Link eventKey="second">New user request</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
          </Row>
          <Row>
            <Col>
              <Tab.Content>
                <Tab.Pane eventKey="first">
                  <OrderCard />
                </Tab.Pane>
                <Tab.Pane eventKey="second">
                  <NewRequestCard />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    </>
  );
}
