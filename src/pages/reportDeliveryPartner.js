import React, { Component, useState } from "react";
import MainTable from "../components/tables/mainTable";
import MainPages from "./mainPages";
import ExportToExcel from "../components/excelDownload/index";
import BootstrapTable from "react-bootstrap-table-next";
import { FaSearch } from "react-icons/fa";
import Pagination from "../components/pagination/pagination";

import DateSelector from "../components/tables/datePicker";
import Buttons from "../components/button/button";
import { Button, Modal, Table, Form, Col, Row } from "react-bootstrap";
import addFile from "../assests/Addfile.png";
import deliveryLicense from "../assests/Driver_license.png";
import ViewModalPopup from "../components/modalPopup";
import { FaPen } from "react-icons/fa";
import { BsFillEyeFill } from "react-icons/bs";

function RetailStore() {
  const head = "Retail Store";
  const buttonhead = "Payment Completed";

  const [viewRetailPopup, setViewRetailPopup] = useState(false);
  const [addCoupon, setAddCoupon] = useState(false);
  const [key, setKey] = useState("1");
  const [lable, setLable] = useState();

  const viewRetailModal = () => {
    setViewRetailPopup(true);
  };

  const retailerLocation = [
    {
      id: 0,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 1,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 2,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 3,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
  ];

  /*------------view order------------------- */
  const bodyContent = () => {
    return (
      <>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-1">
          <p className="color-text order-text"> Order ID : 1001 </p>
          <p className="order-date"> Order Date and Time:10 jan 2022,2:00pm </p>
        </div>
        <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3">
          <p className="order-date">
            {" "}
            Customer name: <span> Raj </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Mobile: <span> 9898978938 </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Email: <span> raj@email.com </span>{" "}
          </p>
        </div>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-5 product-details">
          <table>
            <tr>
              <th>S.No</th>
              <th>Product Name</th>
              <th>MRP</th>
              <th>Quantity</th>
              <th>Amount</th>
            </tr>
            {retailerLocation.map((item) => (
              <tr>
                <td>{item.id + 1} </td>
                <td>{item.productName}</td>
                <td>{item.mrp}</td>
                <td>{item.quantity}</td>
                <td>{item.amount}</td>
              </tr>
            ))}
            <tr className="total-txt">
              <td colspan="4">Total</td>
              <td>Rs.250</td>
            </tr>
          </table>
        </div>
      </>
    );
  };

  function userButtonFormatterView(cell, row) {
    return (
      <>
        <Button
          className="border-none"
          variant="link"
          dataid={cell}
          onClick={() => viewRetailModal()}
        >
          <BsFillEyeFill />
        </Button>
      </>
    );
  }

  const data = [
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "Sai",
      delivery_partner_id: "D0001",
      status: "completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "Raj",
      delivery_partner_id: "D0001",
      status: "pending",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "Sai",
      delivery_partner_id: "D0001",
      status: "completed",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "Raj",
      delivery_partner_id: "D0001",
      status: "cancelled",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "Sai",
      delivery_partner_id: "D0001",
      status: "completed",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "Raj",
      delivery_partner_id: "D0001",
      status: "completed",
      payment_status: "payment completed",
    },
  ];
  const columns = [
    {
      dataField: "order_id",
      text: "Order ID",
    },
    {
      dataField: "Date_Time",
      text: "Date & time",
    },
    {
      dataField: "order_value",
      text: "Order value ",
    },
    {
      dataField: "assigned_to",
      text: " Assigned to",
    },
    {
      dataField: "delivery_partner_id",
      text: "Delivery Partner ID ",
    },
    {
      dataField: "status",
      text: "Status",
    },
    {
      dataField: "id",
      text: "Order item",
      formatter: userButtonFormatterView,
    },
  ];
  return (
    <>
      <div className="p-3 ">
        <div className="retailHeading ">
          <p>Delivery partner</p>
        </div>
      </div>

      <div className="search-button-table  mt-3 ml-4 m-3 ">
        <Row className="picker">
          <Col md={4}>
            <DateSelector />
          </Col>
          <Col md={2}>
            <Form>
              <Form.Group as={Row} className=" mb-3 upload ">
                {/* <Form.Label column sm="3">
            Discount %:
          </Form.Label> */}
                <Col className="retail-dropdown">
                  <Form.Control
                    as="select"
                    name="workout_category_id"
                    // disabled={name === "View"}
                    className="retail-dropdown-option"
                  >
                    <option value="">All</option>
                    <option value="Inprogress">Inprogress</option>
                    <option value="Completed">Completed</option>
                    <option value="Return order">Return order</option>
                  </Form.Control>
                </Col>
              </Form.Group>
            </Form>
          </Col>
          <Col md={2}>
            <ExportToExcel apiData={data} fileName="text" />
          </Col>
        </Row>
        <div className="  d-flex justify-content-between search-border mainButtonHoverBg">
          <div class="main search retail ">
            <div class="form-group has-search ">
              <span class="form-control-feedback">
                <FaSearch size={20} />
              </span>
              <input type="search" class="form-control" placeholder="Search" />
            </div>
          </div>
        </div>

        {viewRetailPopup && (
          <ViewModalPopup
            close={setViewRetailPopup}
            popup={viewRetailPopup}
            heading={"View order"}
            body={bodyContent()}
            size={"lg"}
          />
        )}
        <div className="manage">
          <MainTable data={data} columns={columns} />
        </div>
      </div>
    </>
  );
}
export default RetailStore;
