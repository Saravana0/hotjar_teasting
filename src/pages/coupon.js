import React, { useState } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import ViewModalPopup from "../components/modalPopup";
import { IoMdCloudUpload } from "react-icons/io";
import Buttons from "../components/button/button";
import InputGroup from "react-bootstrap/InputGroup";
import { BiChevronDown } from "react-icons/bi";
import { Tab, Tabs } from "react-bootstrap";
import MainTable from "../components/tables/mainTable";
import { BsFillEyeFill } from "react-icons/bs";
import { FaPen } from "react-icons/fa";
import { FaSearch } from "react-icons/fa";
import ExportToExcel from "../components/excelDownload/index";
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import SingleDateSelector from "../components/tables/singleDatePicker";
import { RiCalendarTodoFill } from "react-icons/ri";

export default function Coupon() {
  const [addCoupon, setAddCoupon] = useState(false);
  const [key, setKey] = useState("1");
  const [lable, setLable] = useState();

  const addCategoryModal = (name) => {
    setAddCoupon(true);
    setLable(name);
  };

  function userButtonFormatter(cell, row) {
    return (
      <>
        <div className="d-flex align-items-center ">
          <Button
            className="border-none pr-2"
            variant="link"
            dataid={cell}
            onClick={() => {
              setAddCoupon(true);
              setLable("View");
            }}
          >
            <BsFillEyeFill />
          </Button>
          {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
          <Button
            className="border-none"
            variant="link"
            dataid={cell}
            onClick={() => {
              setAddCoupon(true);
              setLable("Edit");
            }}
          >
            <FaPen />{" "}
          </Button>
          <div class="custom-control custom-switch toggle-switch">
            <input
              type="checkbox"
              class="custom-control-input"
              id="customSwitches"
            />
            <label class="custom-control-label" for="customSwitches"></label>
          </div>
        </div>
      </>
    );
  }

  const addCouponContent = (name) => {
    return (
      <Form>
        <Form.Group as={Row} className="mb-3 mb-1">
          <Form.Label column sm="3">
            Coupon name:
          </Form.Label>
          <Col sm="7">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Coupon code:
          </Form.Label>
          <Col sm="5">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3 upload">
          <Form.Label column sm="3">
            Start date:
          </Form.Label>

          <Col sm="5" className="coupon-datepicker">
            <label className="w-100">
            <RiCalendarTodoFill className="coupon-RiCalendarTodoFill"/>
            <SingleDateSelector name={name}/>
            </label>
            
          </Col>
        </Form.Group>
        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            End date:
          </Form.Label>
          <Col sm="5" className="coupon-datepicker">
            <label className="w-100">
            <RiCalendarTodoFill className="coupon-RiCalendarTodoFill"/>
            <SingleDateSelector name={name}/>
            </label>
            
          </Col>
        </Form.Group>
        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Min purchase <br />
            amount:
          </Form.Label>
          <Col sm="5">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Max discount <br />
            amount:
          </Form.Label>
          <Col sm="5">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Discount %:
          </Form.Label>
          <Col sm="5">
            <Form.Control
              as="select"
              name="workout_category_id"
              disabled={name === "View"}
            >
              <option value="">10%</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <div className="d-flex justify-content-end modalBtnHoverBg">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg=" m-4  cancel"
                onClick={() => setAddCoupon(false)}
                name="Cancel"
              />
              <Buttons bg=" m-4 save" name={name} />
            </>
          )}
          {name === "View" && (
            <Buttons
              bg=" m-4 save"
              name="close"
              onClick={() => setAddCoupon(false)}
            />
          )}
          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg=" m-4  cancel edit-cancel"
                onClick={() => setAddCoupon(false)}
                name="Cancel"
               
              />
              <Buttons bg=" m-4 save edit-save" name="Update" />
            </>
          )}
        </div>
      </Form>
    );
  };

  const data = [
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "ABC Pharmacy",
      retail_id: "R0001",
      status: "completed",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "ABC Pharmacy",
      retail_id: "R0001",
      status: "completed",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "ABC Pharmacy",
      retail_id: "R0001",
      status: "completed",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "ABC Pharmacy",
      retail_id: "R0001",
      status: "completed",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "ABC Pharmacy",
      retail_id: "R0001",
      status: "completed",
      payment_status: "payment completed",
    },
    {
      order_id: "100001",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to: "ABC Pharmacy",
      retail_id: "R0001",
      status: "completed",
      payment_status: "payment completed",
    },
  ];
  const columns1 = [
    {
      dataField: "sno",
      text: "s.no",
    },
    {
      dataField: "order_id",
      text: "Coupon ID",
    },
    {
      dataField: "Date_Time",
      text: "Coupon text",
    },
    {
      dataField: "order_value",
      text: "Coupon code ",
    },
    {
      dataField: "assigned_to",
      text: " Start date",
    },
    {
      dataField: "retail_id",
      text: "End date ",
    },
    {
      dataField: "status",
      text: "Discount percentage",
    },
    {
      dataField: "status",
      text: "Max discount amount",
    },
    {
      dataField: "id",
      text: "Action",
      formatter: userButtonFormatter,
    },
  ];
  const columns2 = [
    {
      dataField: "order_id",
      text: "Coupon ID",
    },
    {
      dataField: "Date_Time",
      text: "Coupon text",
    },
    {
      dataField: "order_value",
      text: "Coupon code ",
    },
    {
      dataField: "assigned_to",
      text: " Start date",
    },
    {
      dataField: "retail_id",
      text: "End date ",
    },
    {
      dataField: "status",
      text: "Discount percentage",
    },
    {
      dataField: "status",
      text: "Max discount amount",
    },
    {
      dataField: "id",
      text: "Action",
      formatter: userButtonFormatter,
    },
  ];

  return (
    <div className="p-3">
      <div className="retailHeading  pl-2">
        <p>Coupon management</p>
      </div>
      <div className="search-button-table m-3 mb-4 mt-5">
        <div className="coupon-container p-3">
          <Tabs
            id="TabContainer"
            transition={false}
            activeKey={key}
            onSelect={(k) => setKey(k)}
            style={{
              backgroundColor: "white",
            }}
          >
            <Tab eventKey={"1"} title="Active coupons" className="tabBody ">
              {/* <TabBody /> */}
              <hr className="line"></hr>
              <div className=" d-flex justify-content-between align-items-center search-border mainButtonHoverBg">
                <div class="main search retail">
                  <div class="form-group has-search pb-3">
                    <span class="form-control-feedback">
                      <FaSearch size={20} />
                    </span>

                    <input
                      type="search"
                      class="form-control"
                      placeholder="Search"
                    />
                  </div>
                </div>

                <Buttons
                  bg="bg-white mb-3 mr-2 w-60 font"
                  name="Add Coupon"
                  onClick={() => addCategoryModal("Add")}
                />
              </div>
            </Tab>
            <Tab eventKey={"2"} title="Expired coupons" className="tabBody">
              {/* <TabBody /> */}
              <hr className="line"></hr>
              <div className=" d-flex justify-content-between align-items-center ">
                <div class="main search retail">
                  <div class="form-group has-search ">
                    <span class="form-control-feedback">
                      <FaSearch size={20} />
                    </span>
                    <input
                      type="search"
                      class="form-control"
                      placeholder="Search"
                    />
                  </div>
                </div>
              </div>
              {/* <ExportToExcel apiData={data} fileName={"test"} /> */}
            </Tab>
          </Tabs>
        </div>
        <div className="manage">
          <MainTable data={data} columns={columns2} />
        </div>
      </div>

      {addCoupon && (
        <ViewModalPopup
          close={setAddCoupon}
          popup={addCoupon}
          heading={`${lable} coupon`}
          body={addCouponContent(lable)}
          size={"lg"}
        />
      )}
    </div>
  );
}
