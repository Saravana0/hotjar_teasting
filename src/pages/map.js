import React, { Component } from "react";
import GoogleMapReact from "google-map-react";
import { MdClose } from "react-icons/md";
import { Form, Row, Col } from "react-bootstrap";
import { MdLocationOn } from "react-icons/md";
import { useState } from "react";

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const SimpleMap = (props) => {
  const [defaultProps, setdefaultProps] = useState({
    center: {
      lat: 13.04837,
      lng: 80.245483,
    },
    zoom: 11,
  });
  const [area,setArea]=useState("")
  const [landMark,setLandMark]=useState("")

  return (
    <>
      <div className="map-outline">
        <div className={props.name ? "map1" : "map"}>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyBIMGO1PgCfz9s1lAwm0Wq33f6UbUNDDa4 ",
            }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
          >
            <AnyReactComponent
              lat={59.955413}
              lng={30.337844}
              text="My Marker"
            />
          </GoogleMapReact>

          <div className="box">
            <div className="location  ml-2 mr-2">
              <h5>Select your location</h5>
              {/* <MdClose className="icon" onClick={""} /> */}
            </div>

            <form>
              <div class="form-group ml-2 mr-2">
                <div className="currentlocation">
                  <label for="currentlocation">Current location</label>
                  <span>
                    <MdLocationOn className="icon1" />
                  </span>

                  <input
                    type="text"
                    class="form-control pl-4"
                    id="currentLocation"
                    placeholder=""
                  ></input>
                </div>

                <br></br>
                <div class="row">
                  <div class="col">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="Pincode"
                    ></input>
                  </div>
                  <div class="col">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="Block/Shop.no"
                    ></input>
                  </div>
                </div>

                <br></br>
                <div>
                  <div className="areainput">
                    <input
                      type="text"
                      class="form-control"
                      id="area/roadno"
                      placeholder="Area/Road.no"
                      onChange={(e) => setArea(e.target.value)}
                    ></input>
                    {area.length < 1 && (
                      <div className="optional-1 ">
                        <span>Optional</span>
                      </div>
                    )}
                  </div>
                  <br></br>
                  <div className="landmarkinput">
                    <textarea
                      type="text"
                      class="form-control"
                      id="nearbylandmark"
                      rows="3"
                      placeholder="Nearby landmark"
                      onChange={(e) => setLandMark(e.target.value)}
                    ></textarea>
                    {landMark.length < 1 && (
                      <div className="optional-2 ">
                        <span>Optional</span>
                      </div>
                    )}
                  </div>
                  <div className="textbox-disable">
                    <p>{landMark.length}/50</p>
                  </div>
                </div>
              </div>
              <div className="button">
                <button type="submit" class="btn proceed btn-primary">
                  {props.buttonname ? props.buttonname : "Save and proceed"}
                </button>
              </div>
            </form>

            <br></br>
          </div>
        </div>
      </div>
    </>
  );
};


export default SimpleMap;

