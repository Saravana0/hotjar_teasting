import React, { useState, useEffect, useRef } from "react";
// import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import MainTable from "../components/tables/mainTable";
import Pagination from "../components/pagination/pagination";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import { Button, Modal, Table, Form, Col, Row } from "react-bootstrap";
import ViewModalPopup from "../components/modalPopup";
import BootstrapTable from "react-bootstrap-table-next";
import { FaSearch } from "react-icons/fa";
import { Link } from "react-router-dom";
import { BsFillEyeFill } from "react-icons/bs";
import { FaPen } from "react-icons/fa";

function btnFormatter(cell, row, id) {
  return (
    <>
      <Link
      // variant="link"
      // to={{
      //   pathname: `/manage-workout/add-workout/`,
      //   state: {
      //     workout_master_id: row.id,
      //     workout_category_name: row.workout_category_name,
      //     editAccess: false,
      //     edit: false,
      //     duplicate: false,
      //   },
      // }}
      >
        {/* <FontAwesomeIcon icon={faEye} /> */}

        <div className="eye_icon">
          <BsFillEyeFill />
        </div>
      </Link>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <Link>
        <div className="order_button">
          <Button variant="success">Order Reafewsfdy</Button>{" "}
        </div>
      </Link>
    </>
  );
}

const datain = [
  {
    s_no: 1,
    order_no: "#12345",
    customer_name: "Justin Septimus",
    mobile: "9999999999",
    status: "Order accepted",
    delivery_partner_name: "Bharath Raj",
    mobile: "9999999999",
    action: "Action",
  },
  {
    s_no: 2,
    order_no: "#12345",
    customer_name: "Justin Septimus",
    mobile: "9999999999",
    status: "Order accepted",
    delivery_partner_name: "Bharath Raj",
    mobile: "9999999999",
    action: "Action",
  },
  {
    s_no: 3,
    order_no: "#12345",
    customer_name: "Justin Septimus",
    mobile: "9999999999",
    status: "Order accepted",
    delivery_partner_name: "Bharath Raj",
    mobile: "9999999999",
    action: "Action",
  },
  {
    s_no: 4,
    order_no: "#12345",
    customer_name: "Justin Septimus",
    mobile: "9999999999",
    status: "Order accepted",
    delivery_partner_name: "Bharath Raj",
    mobile: "9999999999",
    action: "Action",
  },
  {
    s_no: 5,
    order_no: "#12345",
    customer_name: "Justin Septimus",
    mobile: "9999999999",
    status: "Order accepted",
    delivery_partner_name: "Bharath Raj",
    mobile: "9999999999",
    action: "Action",
  },
  {
    s_no: 6,
    order_no: "#12345",
    customer_name: "Justin Septimus",
    mobile: "9999999999",
    status: "Order accepted",
    delivery_partner_name: "Bharath Raj",
    mobile: "9999999999",
    action: "Action",
  },
];
const columnsin = [
  {
    dataField: "s_no",
    text: "S.No",
  },
  {
    dataField: "order_no",
    text: "Order No",
  },
  {
    dataField: "customer_name",
    text: "Customer Name",
  },
  ,
  {
    dataField: "mobile",
    text: "Mobile",
  },
  {
    dataField: "status",
    text: "Status",
  },
  {
    dataField: "delivery_partner_name",
    text: "Delivery Partner Name",
  },
  {
    dataField: "mobile",
    text: "Mobile",
  },
  {
    dataField: "action",
    text: "Action",
    formatter: btnFormatter,
  },
];

const data = [
  {
    retailer_name: "Raj Kumar",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Varun",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Hema",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Dinesh",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Dheena",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Bhavana",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Sneha",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Varun",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Ajith",
    location: "Adayar, Chennai",
  },
  {
    retailer_name: "Priya",
    location: "Adayar, Chennai",
  },
];

const columns = [
  {
    dataField: "retailer_name",
    text: "Retailer Name",
  },
  {
    dataField: "location",
    text: "Location",
  },
];

export default function PopupTable(props) {
  const [showPopup, setShowPopup] = useState(false);
  const [assignRetail, setAssignRetail] = useState(false);
  const [inprogressOrder, setInprogressOrder] = useState(false);

  const assignRetailModal = () => {
    setAssignRetail(true);
  };
  const inprogressOrderModal = () => {
    setInprogressOrder(true);
  };

  const assignRetailContent = () => {
    return (
      <>
        <div class="main search retail popupSearch">
          <div class="form-group has-search">
            <span class="form-control-feedback">
              <FaSearch />
            </span>
            <input
              type="text"
              class="form-control"
              placeholder="Search by retailor name"
            />
          </div>
        </div>

        <div className="assain_retail">
          <MainTable data={data} columns={columns} />
          {/* <TableSelect
              clickSelect="true"
              data={tableList}
              offset={filter ? offset : offset}
              // resetSelectRow={resetSelectRow}
              ref={tableRef}
              tableCount={tableCount}
              showAllRecords={showAllRecords}
              showButton={showButton}
              // columns={columnss}
              // filters={tableFilters}
              // getSelectedRow={getSelectedRow}
              searchBar="false"
            /> */}
        </div>
      </>
    );
  };

  const inProgressContent = () => {
    return (
      <>
        <div className="assain_retail">
          <MainTable data={datain} columns={columnsin} />
          {/* <TableSelect
              clickSelect="true"
              data={tableList}
              offset={filter ? offset : offset}
              // resetSelectRow={resetSelectRow}
              ref={tableRef}
              tableCount={tableCount}
              showAllRecords={showAllRecords}
              showButton={showButton}
              // columns={columnss}
              // filters={tableFilters}
              // getSelectedRow={getSelectedRow}
              searchBar="false"
            /> */}
        </div>
      </>
    );
  };

  return (
    <>
      <div>
        <Button onClick={assignRetailModal} style={{ marginLeft: 20 }}>
          {" "}
          Assign Retail{" "}
        </Button>
        {assignRetail && (
          <ViewModalPopup
            close={setAssignRetail}
            popup={assignRetail}
            heading={"Assign Retail"}
            body={assignRetailContent()}
            size={"lg"}
          />
        )}

        <Button onClick={inprogressOrderModal} style={{ marginLeft: 20 }}>
          {" "}
          Assign Retail{" "}
        </Button>
        {inprogressOrder && (
          <ViewModalPopup
            close={setInprogressOrder}
            popup={inprogressOrder}
            heading={"Inprogress Order"}
            body={inProgressContent()}
            size={"xl"}
          />
        )}
      </div>
    </>
  );
}
