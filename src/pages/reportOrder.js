import React, { useState } from "react";
import { Button, Modal, Table, Form, Col, Row } from "react-bootstrap";
import ViewModalPopup from "../components/modalPopup";
import MainTable from "../components/tables/mainTable";
import OrderHistory from "../components/orderHistory/orderHistory";
import { FaSearch } from "react-icons/fa";
import { BsFillEyeFill } from "react-icons/bs";
import { NavLink, Link } from "react-router-dom";
import ExportToExcel from "../components/excelDownload/index";
import DateSelector from "../components/tables/datePicker";

export default function Coupon() {
  const [addCoupon, setAddCoupon] = useState(false);
  const [viewRetailPopup, setViewRetailPopup] = useState(false);
  const [lable, setLable] = useState();
  const addCategoryModal = (name) => {
    setAddCoupon(true);
    setLable(name);
  };
  const viewRetailModal = () => {
    setViewRetailPopup(true);
  };

  const retailerLocation = [
    {
      id: 0,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 1,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 2,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
    {
      id: 3,
      productName: "Rajkumar",
      mrp: 324,
      quantity: 2,
      amount: 648,
    },
  ];

  const bodyContent = () => {
    return (
      <>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-1">
          <p className="color-text order-text"> Order ID : 1001 </p>
          <p className="order-date"> Order Date and Time:10 jan 2022,2:00pm </p>
        </div>
        <div className="d-flex justify-content-between retailStore-detail  ml-3 mb-3">
          <p className="order-date">
            {" "}
            Customer name: <span> Raj </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Mobile: <span> 9898978938 </span>{" "}
          </p>
          <p className="order-date">
            {" "}
            Email: <span> raj@email.com </span>{" "}
          </p>
        </div>
        <div className="d-flex justify-content-between mr-3 ml-3 mb-5 product-details">
          <table>
            <tr>
              <th>S.No</th>
              <th>Product Name</th>
              <th>MRP</th>
              <th>Quantity</th>
              <th>Amount</th>
            </tr>
            {retailerLocation.map((item) => (
              <tr>
                <td>{item.id + 1} </td>
                <td>{item.productName}</td>
                <td>{item.mrp}</td>
                <td>{item.quantity}</td>
                <td>{item.amount}</td>
              </tr>
            ))}
            <tr className="total-txt">
              <td colspan="4">Total</td>
              <td>Rs.250</td>
            </tr>
          </table>
        </div>
      </>
    );
  };

  function userLinkFormatterView(cell, row) {
    return (
      <>
        <Button className="image border-none" onClick={() => viewRetailModal()}>
          View Order
        </Button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <Button
          className="image border-none"
          onClick={() => addCategoryModal()}
        >
          View Details
        </Button>
      </>
    );
  }

  const data = [
    {
      order_id: "100001",
      order_status: "Delivered",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to_retail_store: "ABC Pharmacy",
      retail_id: "R0001",
      payment_status: "payment completed",
      assigned_to_delivery_partner: "Raj",
      delivery_partner_id: "D0001",
      payment_status: "Payment Completed",
    },
    {
      order_id: "100001",
      order_status: "Delivered",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to_retail_store: "ABC Pharmacy",
      retail_id: "R0001",
      payment_status: "payment completed",
      assigned_to_delivery_partner: "Raj",
      delivery_partner_id: "D0001",
      payment_status: "Payment Completed",
    },
    {
      order_id: "100001",
      order_status: "Delivered",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to_retail_store: "ABC Pharmacy",
      retail_id: "R0001",
      payment_status: "payment completed",
      assigned_to_delivery_partner: "Raj",
      delivery_partner_id: "D0001",
      payment_status: "Payment Completed",
    },
    {
      order_id: "100001",
      order_status: "Delivered",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to_retail_store: "ABC Pharmacy",
      retail_id: "R0001",
      payment_status: "payment completed",
      assigned_to_delivery_partner: "Raj",
      delivery_partner_id: "D0001",
      payment_status: "Payment Completed",
    },
    {
      order_id: "100001",
      order_status: "Delivered",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to_retail_store: "ABC Pharmacy",
      retail_id: "R0001",
      payment_status: "payment completed",
      assigned_to_delivery_partner: "Raj",
      delivery_partner_id: "D0001",
      payment_status: "Payment Completed",
    },
    {
      order_id: "100001",
      order_status: "Delivered",
      Date_Time: "10 Jan 2022,2.00pm",
      order_value: "Rs.100",
      assigned_to_retail_store: "ABC Pharmacy",
      retail_id: "R0001",
      payment_status: "payment completed",
      assigned_to_delivery_partner: "Raj",
      delivery_partner_id: "D0001",
      payment_status: "Payment Completed",
    },
  ];
  const columns = [
    {
      dataField: "order_id",
      text: "Order ID",
    },
    {
      dataField: "order_status",
      text: "Order status ",
    },
    {
      dataField: "Date_Time",
      text: "Date & time",
    },
    {
      dataField: "order_value",
      text: "Order value ",
    },
    {
      dataField: "assigned_to_retail_store",
      text: " Assigned to (Retail store)",
    },
    {
      dataField: "retail_id",
      text: "Retail ID",
    },
    {
      dataField: "payment_status",
      text: "Payment status",
    },
    {
      dataField: "assigned_to_delivery_partner",
      text: " Assigned to (Delivery partner)",
    },
    {
      dataField: "delivery_partner_id",
      text: "Delivery partner ID",
    },
    {
      dataField: "payment_status",
      text: "Payment status",
    },
    {
      dataField: "action",
      text: "Action",
      formatter: userLinkFormatterView,
    },
  ];

  const addCouponContent = (name) => {
    return (
      <>
        <OrderHistory />
      </>
    );
  };
  return (
    <>
      <div className="p-3">
        <div className="retailHeading ">
          <p>Orders</p>
        </div>
      </div>
      <div className="search-button-table mt-3 ml-4 m-3 ">
      <Row>
          <Col md={4}>
            <DateSelector />
          </Col>
          <Col md={2}>
            <Form>
              <Form.Group as={Row} className=" mb-3 upload ">
                {/* <Form.Label column sm="3">
            Discount %:
          </Form.Label> */}
                <Col className="retail-dropdown">
                  <Form.Control
                    as="select"
                    name="workout_category_id"
                    // disabled={name === "View"}
                    className="retail-dropdown-option"
                  >
                    <option value="">All</option>
                    <option value="Inprogress">Inprogress</option>
                    <option value="Completed">Completed</option>
                    <option value="Return order">Return order</option>
                  </Form.Control>
                </Col>
              </Form.Group>
            </Form>
          </Col>
          <Col md={2}>
            <ExportToExcel apiData={data} fileName="text" />
          </Col>
        </Row>
        <div className=" d-flex justify-content-between search-border">
          <div class="main search retail ">
            <div class="form-group has-search pt-4 pb-2">
              <span class="form-control-feedback">
                <FaSearch size={20} />
              </span>
              <input type="search" class="form-control" placeholder="Search" />
            </div>
          </div>
        </div>

        {addCoupon && (
          <ViewModalPopup
            close={setAddCoupon}
            popup={addCoupon}
            heading={"Order History"}
            body={addCouponContent(lable)}
            size={"lg"}
          />
        )}
        {viewRetailPopup && (
          <ViewModalPopup
            close={setViewRetailPopup}
            popup={viewRetailPopup}
            heading={"View order"}
            body={bodyContent()}
            size={"lg"}
          />
        )}
        <div className="manage">
          <MainTable data={data} columns={columns} />
        </div>
      </div>
    </>
  );
}
