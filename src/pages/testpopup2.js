import React, { useState, Component } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import ViewModalPopup from "../components/modalPopup";
import { IoMdCloudUpload } from "react-icons/io";
import Buttons from "../components/button/button";




export default function Dashboard() {
  const [addCategory, setAddCategory] = useState(false);
  const [editCategory, setEditCategory] = useState(false);
  const [addProduct, setAddProduct] = useState(false);
  const [lable, setLable] = useState();

  const addCategoryModal = (name) => {
    setAddCategory(true);
    setLable(name);
  };

  const addProductModal = (name) => {
    setAddProduct(true);
    setLable(name);
  };

  const addCategoryContent = (name) => {
    return (
      <Form>
        <Form.Group as={Row} className="mb-3 mb-1">
          <Form.Label column sm="3">
            Category name:
          </Form.Label>
          <Col sm="7">
            <Form.Control type="text" placeholder="" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Add image:
          </Form.Label>
          <Col
            className="ml-4"
            sm="3"
            className={
              "image"
                ? "imgUploadBox d-flex align-items-center justify-content-center mwx-400"
                : "imgUploadBox d-flex align-items-center justify-content-center mwx-400 ml-auto mr-auto "
            }
          >
            <input type="file" name="avatar" accept="image/*" />
            <IoMdCloudUpload />
          </Col>
        </Form.Group>

        <div className="d-flex justify-content-end ">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name="Save" />
            </>
          )}

          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name="Update" />
            </>
          )}
        </div>
      </Form>
    );
  };

  /*---------------------Add-product------------------*/

  const addProductContent = (name) => {
    return (
      <Form>
        <Form.Group as={Row} className="mb-3 mb-1">
          <Form.Label column sm="3">
            Product name:
          </Form.Label>
          <Col sm="7">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3 mb-1">
          <Form.Label column sm="3">
            Select category:
          </Form.Label>
          <Col sm="7">
            <Form.Select className="select-category" disabled={name === "View"}>
              <option></option>
            </Form.Select>
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-1 ">
          <Form.Label column sm="3">
            Descriptions:
          </Form.Label>
          <Col sm="7">
            <Form.Group
              className="mb-0"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Control as="textarea" rows={3} disabled={name === "View"} />
            </Form.Group>
            <p className="float-right mt-0">0/240</p>
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3">
          <Form.Label column sm="3">
            Other comments:
          </Form.Label>

          <Col sm="7">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3">
          <Form.Label column sm="3">
            Price(MRP):
          </Form.Label>
          <Col sm="7">
            <Form.Control
              type="text"
              placeholder=""
              disabled={name === "View"}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className=" mb-3 upload ">
          <Form.Label column sm="3">
            Add image:
          </Form.Label>
          <Col
            className="ml-4"
            sm="3"
            className={
              "image"
                ? "imgUploadBox d-flex align-items-center justify-content-center mwx-400"
                : "imgUploadBox d-flex align-items-center justify-content-center mwx-400 ml-auto mr-auto "
            }
          >
            <input
              type="file"
              name="avatar"
              accept="image/*"
              disabled={name === "View"}
            />
            <IoMdCloudUpload />
          </Col>
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Check type="checkbox" label="Prescription required" />
        </Form.Group>

        <div className="d-flex justify-content-end ">
          {name === "Add" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name="Add" />
            </>
          )}
          {name === "View" && (
            <>
              {" "}
              <Buttons bg="bg-red m-4 save" name="close" />
            </>
          )}

          {name === "Edit" && (
            <>
              {" "}
              <Buttons
                bg="bg-red m-4  cancel"
                onClick={() => setAddCategory(false)}
                name="Cancel"
              />
              <Buttons bg="bg-red m-4 save" name="Update" />
            </>
          )}
        </div>
      </Form>
    );
  };

  return (
    <div >
      <Button
        onClick={() => addCategoryModal("Add")}
        style={{ marginLeft: 20 }}
      >
        Add Category{" "}
      </Button>
      <Button
        onClick={() => addCategoryModal("Edit")}
        style={{ marginLeft: 20 }}
      >
        Edit Category{" "}
      </Button>
      {addCategory && (
        <ViewModalPopup
          close={setAddCategory}
          popup={addCategory}
          heading={`${lable}  Category`}
          body={addCategoryContent(lable)}
          size={"lg"}

        />
      )}

      <Button onClick={() => addProductModal("Add")} style={{ marginLeft: 20 }}>
        Add Product{" "}
      </Button>
      <Button
        onClick={() => addProductModal("Edit")}
        style={{ marginLeft: 20 }}
      >
        Edit Product{" "}
      </Button>
      <Button
        onClick={() => addProductModal("View")}
        style={{ marginLeft: 20 }}
      >
        View Product{" "}
      </Button>
      {addProduct && (
        <ViewModalPopup
          close={setAddProduct}
          popup={addProduct}
          heading={`${lable} Product`}
          body={addProductContent(lable)}
          size={"lg"}
        />
      )}
    </div>
  );
}
