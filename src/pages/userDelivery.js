import React, { useState, useEffect, useRef } from "react";
// import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import MainTable from "../components/tables/mainTable";
import Pagination from "../components/pagination/pagination";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import TableSelect from "../components/tables/mainTable";
import { BsSearch } from "react-icons/bs";
import { FaSearch } from "react-icons/fa";
import { Link } from "react-router-dom";
import { BsFillEyeFill } from "react-icons/bs";
import { FaPen } from "react-icons/fa";

function btnFormatter(cell, row, id) {
  return (
    <>
      <Link
      // variant="link"
      // to={{
      //   pathname: `/manage-workout/add-workout/`,
      //   state: {
      //     workout_master_id: row.id,
      //     workout_category_name: row.workout_category_name,
      //     editAccess: false,
      //     edit: false,
      //     duplicate: false,
      //   },
      // }}
      >
        {/* <FontAwesomeIcon icon={faEye} /> */}
        <BsFillEyeFill />
      </Link>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <Link
      // variant="link"
      // to={{
      //   pathname: `/manage-workout/add-workout/`,
      //   state: {
      //     workout_master_id: row.id,
      //     workout_category_name: row.workout_category_name,
      //     edit: false,
      //     duplicate: false,
      //   },
      // }}
      >
        <FaPen />
      </Link>
      {/* <Form.Check
            type="switch"
            id={`toggle-switch-${cell}`}
            checked={row.is_active == 0 ? false : true}
            // onChange={() => userStatusChange(cell, row.is_active)}
          /> */}
      {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; */}
      <div class="custom-control custom-switch toggle-switch">
        <input
          type="checkbox"
          class="custom-control-input"
          id="customSwitches"
        />
        <label class="custom-control-label" for="customSwitches"></label>
      </div>
    </>
  );
}

const data = [
  {
    s_no: 1,
    id: "R00001",
    name: "Raj",
    mobile_num: 9876543210,
    e_mail: "susan@mail.com",
    address: "adayar,chennai",
    status: "Active",
  },
  {
    s_no: 2,
    id: "R00001",
    name: "Raj",
    mobile_num: 9876543210,
    e_mail: "susan@mail.com",
    address: "adayar,chennai",
    status: "Inactive",
  },
  {
    s_no: 3,
    id: "R00001",
    name: "Raj",
    mobile_num: 9876543210,
    e_mail: "susan@mail.com",
    address: "adayar,chennai",
    status: "Active",
  },
  {
    s_no: 4,
    id: "R00001",
    name: "Raj",
    mobile_num: 9876543210,
    e_mail: "susan@mail.com",
    address: "adayar,chennai",
    status: "Inactive",
  },
  {
    s_no: 5,
    id: "R00001",
    name: "Raj",
    mobile_num: 9876543210,
    e_mail: "susan@mail.com",
    address: "adayar,chennai",
    status: "Pending",
  },
  {
    s_no: 6,
    id: "R00001",
    name: "Raj",
    mobile_num: 9876543210,
    e_mail: "susan@mail.com",
    address: "adayar,chennai",
    status: "Active",
  },
];
const columns = [
  {
    dataField: "s_no",
    text: "S.No",
  },
  {
    dataField: "id",
    text: "ID",
  },
  {
    dataField: "name",
    text: "Name",
  },
  ,
  {
    dataField: "mobile_num",
    text: "Mobile number",
  },
  {
    dataField: "e_mail",
    text: "Email",
  },
  {
    dataField: "address",
    text: "Address",
  },
  {
    dataField: "status",
    text: "Status",
  },
  {
    dataField: "action",
    text: "Action",
    formatter: btnFormatter,
  },
];

export default function UserDelivery(props) {
  return (
    <>
      <div className="p-4">
        <div className="retailHeading">
          <p>Delivery Partner</p>
        </div>

        <div className="search-button-table">
          <div className="search-and-add">
            <div class="main search retail">
              <div class="form-group has-search">
                <span class="form-control-feedback">
                  <FaSearch />
                </span>
                <input type="text" class="form-control" placeholder="Search" />
              </div>
            </div>

            <div className="add-delivery">
              <button type="button">Add delivery partner</button>
            </div>
          </div>

          <div>
            <MainTable data={data} columns={columns} />
            {/* <TableSelect
              clickSelect="true"
              data={tableList}
              offset={filter ? offset : offset}
              // resetSelectRow={resetSelectRow}
              ref={tableRef}
              tableCount={tableCount}
              showAllRecords={showAllRecords}
              showButton={showButton}
              // columns={columnss}
              // filters={tableFilters}
              // getSelectedRow={getSelectedRow}
              searchBar="false"
            /> */}
          </div>
        </div>

        <div>
          <Pagination />
        </div>
      </div>
    </>
  );
}
