import React, { useState } from "react";
import { Route, Redirect, useLocation } from "react-router-dom";
import SideBar from "../components/sidebar";
import Header from "../components/header";
import Hamburger from "hamburger-react";
import Menu from "../components/menu/menu";
export default function PrivateRoute({
  component: Component,
  getMenuList,
  menuList,
  ...rest
}) {
  const [isOpen, setOpen] = useState(false);
  return (
    <Route
      {...rest}
      render={(props) => (
        <>
          <div className="hamburger-menu">
            <Hamburger onToggle={() => setOpen(!isOpen)} />
            {isOpen && <Menu />}
          </div>
          <Header />
          <main className="dashboardMain">
            <SideBar />
            <section className="rightContent">
              <Component {...props} />
            </section>
          </main>
        </>
      )}
    />
  );
}
